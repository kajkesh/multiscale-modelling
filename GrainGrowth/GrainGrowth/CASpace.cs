﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GrainGrowth
{
    class CASpace
    {
        private Graphics SpaceGraphics;
        private Graphics EnergyGraphics;
        private Bitmap SpaceBitmap;
        private Bitmap EnergyBitmap;
        private static readonly Random GetRandom = new Random();
        public Cell[,] PreviousState;
        public Cell[,] CurrentState;
        public Dictionary<int, SolidBrush> ColorsMap;
        public Dictionary<int, SolidBrush> EnergyColorsMap;
        private NeighborhoodType TypeOfNeighborhood;
        private int Probability;

        private int Width;
        private int Height;

        public CASpace(int width, int height)
        {
            Width = width;
            Height = height;
            Initialize();
        }

        public int GetWidth()
        {
            return Width;
        }

        public int GetHeight()
        {
            return Height;
        }

        public void SetProbability(int value)
        {
            Probability = value;
        }

        public void SetNeighborhood(NeighborhoodType type)
        {
            TypeOfNeighborhood = type;
        }

        private void Initialize()
        {
            SpaceBitmap = new Bitmap(Height * 2, Width * 2);
            SpaceGraphics = Graphics.FromImage(SpaceBitmap);
            EnergyBitmap = new Bitmap(Height, Width);
            EnergyGraphics = Graphics.FromImage(EnergyBitmap);
            PreviousState = new Cell[Width, Height];
            CurrentState = new Cell[Width, Height];
            ColorsMap = new Dictionary<int, SolidBrush>();
            EnergyColorsMap = new Dictionary<int, SolidBrush>();
            Probability = 100;
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {
                    PreviousState[i, j] = new Cell(i, j, -1);
                    CurrentState[i, j] = new Cell(i, j, -1);
                }
            }
        }

        public bool IsColorForGrain(int grainNumber)
        {
            if (ColorsMap.TryGetValue(grainNumber, out SolidBrush brush))
                return true;
            else
                return false;
        }

        public int GetGrainNumberForColor(Color color)
        {
            foreach (KeyValuePair<int, SolidBrush> entry in ColorsMap)
            {
                if (entry.Value.Color == color)
                    return entry.Key;
            }
            return -1;
        }

        public void AddNewBrush(int grainNumber)
        {
            int r, g, b;
            r = GetRandom.Next(10, 245);
            g = GetRandom.Next(10, 245);
            b = GetRandom.Next(10, 245);
            Color color = Color.FromArgb(r, g, b);
            ColorsMap.Add(grainNumber, new SolidBrush(color));
        }

        public void AddRecrystallizedBrush(int grainNumber)
        {
            int r, g, b;
            r = GetRandom.Next(0, 255);
            g = 0;
            b = 0;
            Color color = Color.FromArgb(r, g, b);
            ColorsMap.Add(grainNumber, new SolidBrush(color));
        }

        public int AddNewBrush(Color color)
        {
            ColorsMap.Add(ColorsMap.Count, new SolidBrush(color));
            return ColorsMap.Count - 1;
        }

        public void ZeroTheSpace()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    CurrentState[i, j].ZeroTheCell();
                    PreviousState[i, j].ZeroTheCell();
                }
            }
            ColorsMap.Clear();
        }

        public void CreateNewNucleon()
        {
            bool placeForNucleonFound = false;
            while (!placeForNucleonFound)
            {
                int i = GetRandom.Next(0, Height);
                int j = GetRandom.Next(0, Width);
                if (CurrentState[i, j].GetGrainNumber() == -1)
                {
                    placeForNucleonFound = true;
                    CurrentState[i, j].SetGrainNumber(ColorsMap.Count);
                    AddNewBrush(ColorsMap.Count);
                }
            }
        }

        public Bitmap DrawTheSpace()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    int id = CurrentState[i, j].GetGrainNumber();
                    Rectangle rectangle = new Rectangle(j * 2, i * 2, 2, 2);
                    if (id > -1 || id < -2)
                    {
                        SpaceGraphics.FillRectangle(ColorsMap[id], rectangle);
                    }
                    else if (id == -1)
                    {
                        SpaceGraphics.FillRectangle(new SolidBrush(Color.White), rectangle);
                    }
                    else
                    {
                        SpaceGraphics.FillRectangle(new SolidBrush(Color.Black), rectangle);
                    }
                }
            }
            return SpaceBitmap;
        }

        public Bitmap DrawTheEnergy()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    int energyValue = CurrentState[i, j].GetEnergy();
                    Rectangle rectangle = new Rectangle(j, i, 1, 1);
                    EnergyGraphics.FillRectangle(EnergyColorsMap[energyValue], rectangle);
                }
            }
            return EnergyBitmap;
        }

        public void SetNeighborhood()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    PreviousState[i, j].SetNeighbors(Width, Height);
                    CurrentState[i, j].SetNeighbors(Width, Height);
                }
            }
        }

        public void MonteCarloIteration()
        {
            List<Point> coordinatesList = new List<Point>();
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    coordinatesList.Add(new Point(i, j));
                }
            }
            while (coordinatesList.Count > 0)
            {
                int randomIndex = GetRandom.Next(0, coordinatesList.Count);
                Point position = coordinatesList[randomIndex];
                coordinatesList.RemoveAt(randomIndex);

                if (CurrentState[position.X, position.Y].GetGrainNumber() >= -1)
                    CurrentState[position.X, position.Y].RandomNewGrainNumber(CurrentState);
            }
        }

        public bool RecrystallizationIteration(int numberOfNucleonsToAdd = 0, bool nucleonsOnBorder = false)
        {
            int numberOfFreeCellsForNucleons = 0;
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (!CurrentState[i, j].IsCellRecrystallized() && (!nucleonsOnBorder || CurrentState[i, j].IsCellOnBorder()))
                        numberOfFreeCellsForNucleons++;
                }
            }

            if (numberOfFreeCellsForNucleons >= numberOfNucleonsToAdd)
            {
                AddRecrystallizedNucleons(numberOfNucleonsToAdd, nucleonsOnBorder);
                DrawTheEnergy();
                DrawTheSpace();
            }

            List<Point> coordinatesList = new List<Point>();
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (!CurrentState[i, j].IsCellRecrystallized())
                        coordinatesList.Add(new Point(i, j));
                }
            }

            if (coordinatesList.Count == 0)
                return false;

            while (coordinatesList.Count > 0)
            {
                int randomIndex = GetRandom.Next(0, coordinatesList.Count);
                Point position = coordinatesList[randomIndex];
                coordinatesList.RemoveAt(randomIndex);

                int oldGrainNumber = CurrentState[position.X, position.Y].GetGrainNumber();
                CurrentState[position.X, position.Y].RecrystallizedNewGrainNumber(CurrentState);

                if (CurrentState[position.X, position.Y].GetGrainNumber() != oldGrainNumber)
                {
                    CurrentState[position.X, position.Y].SetRecrystallized();
                    CurrentState[position.X, position.Y].SetEnergy(0);
                }
            }

            return true;
        }

        public bool Iteration()
        {
            RewriteStates();
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (CurrentState[i, j].GetGrainNumber() == -1)
                    {
                        if (TypeOfNeighborhood == NeighborhoodType.VonNeumann)
                            CurrentState[i, j].SetGrainNumber(PreviousState[i, j].CalculateNewGrainNumber(PreviousState, NeighborhoodType.VonNeumann, 1));
                        else
                        {
                            CurrentState[i, j].SetGrainNumber(PreviousState[i, j].CalculateNewGrainNumber(PreviousState, NeighborhoodType.Moore, 5));
                            if (CurrentState[i, j].GetGrainNumber() == -1)
                                CurrentState[i, j].SetGrainNumber(PreviousState[i, j].CalculateNewGrainNumber(PreviousState, NeighborhoodType.VonNeumann, 3));
                            if (CurrentState[i, j].GetGrainNumber() == -1)
                                CurrentState[i, j].SetGrainNumber(PreviousState[i, j].CalculateNewGrainNumber(PreviousState, NeighborhoodType.FurtherMoore, 3));
                            if (CurrentState[i, j].GetGrainNumber() == -1)
                            {
                                if (GetRandom.Next(0, 100) <= Probability)
                                    CurrentState[i, j].SetGrainNumber(PreviousState[i, j].CalculateNewGrainNumber(PreviousState, NeighborhoodType.Moore, 1));
                            }
                        }
                    }
                    else
                    {
                        CurrentState[i, j].SetGrainNumber(PreviousState[i, j].GetGrainNumber());

                    }
                }
            }
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (CurrentState[i, j].GetGrainNumber() != PreviousState[i, j].GetGrainNumber())
                        return true;
                }
            }
            return false;
        }

        public void RewriteStates()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    PreviousState[i, j].SetGrainNumber(CurrentState[i, j].GetGrainNumber());
                }
            }
        }

        public bool IsFull()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (CurrentState[i, j].GetGrainNumber() == -1)
                        return false;
                }
            }
            return true;
        }

        public void CreateNewInclusion(int dimension, bool isInclusionSquare, bool isOnBorder = false)
        {
            bool placeForInclusionFound = false;
            while (!placeForInclusionFound)
            {
                int i = GetRandom.Next(0 + dimension, Height - dimension);
                int j = GetRandom.Next(0 + dimension, Width - dimension);
                if ((isOnBorder && PreviousState[i, j].IsCellOnBorder()) || PreviousState[i, j].GetGrainNumber() == -1)
                {
                    placeForInclusionFound = true;
                    for (int n = (-1) * dimension; n <= dimension; ++n)
                    {
                        for (int m = (-1) * dimension; m <= dimension; ++m)
                        {
                            if (isInclusionSquare)
                            {
                                CurrentState[i + n, j + m].SetGrainNumber(-2);
                                PreviousState[i + n, j + m].SetGrainNumber(-2);
                            }
                            else if (MathUtils.EuclideanDistance(i, j, i + n, j + m) <= dimension)
                            {
                                CurrentState[i + n, j + m].SetGrainNumber(-2);
                                PreviousState[i + n, j + m].SetGrainNumber(-2);
                            }
                        }
                    }
                }
            }
        }

        public void SetTheBorders()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    PreviousState[i, j].CheckBorders(PreviousState);
                    CurrentState[i, j].CheckBorders(CurrentState);
                }
            }
        }

        public void SelectTheGrains(System.Windows.Forms.ListBox.ObjectCollection list, bool isDualPhase = false)
        {
            Dictionary<int, SolidBrush> newColorsMap = new Dictionary<int, SolidBrush>();
            int newId = -3;
            for (int i = 0; i < list.Count; ++i)
            {
                int oldId = Convert.ToInt32(list[i]);
                if (!isDualPhase || i == 0)
                {
                    newColorsMap.Add(newId, ColorsMap[oldId]);
                }

                for (int x = 0; x < Height; ++x)
                {
                    for (int y = 0; y < Width; ++y)
                    {
                        if (PreviousState[x, y].GetGrainNumber() == oldId)
                        {
                            CurrentState[x, y].SetGrainNumber(newId);
                            PreviousState[x, y].SetGrainNumber(newId);
                        }
                    }
                }

                if (!isDualPhase)
                    newId--;
            }
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (PreviousState[i, j].GetGrainNumber() > -3)
                    {
                        CurrentState[i, j].ZeroTheCell();
                        PreviousState[i, j].ZeroTheCell();
                    }
                }
            }
            ColorsMap.Clear();
            ColorsMap = newColorsMap;
        }

        public void DrawTheBorders(System.Windows.Forms.ListBox.ObjectCollection list)
        {
            int numberOfGrainsSelected = list.Count;
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (numberOfGrainsSelected > 0 && !list.Contains(PreviousState[i, j].GetGrainNumber().ToString()))
                    {
                        continue;
                    }
                    bool isGrainBorder = numberOfGrainsSelected > 0 ? PreviousState[i, j].CheckIfIsBorder(PreviousState, true) : PreviousState[i, j].CheckIfIsBorder(PreviousState);
                    if (isGrainBorder)
                    {
                        PreviousState[i, j].SetGrainNumber(-2);
                        CurrentState[i, j].SetGrainNumber(-2);
                    }
                }
            }
        }

        public void ClearTheBorders()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (PreviousState[i, j].GetGrainNumber() != -2)
                    {
                        CurrentState[i, j].ZeroTheCell();
                        PreviousState[i, j].ZeroTheCell();
                    }
                }
            }
        }

        public double CalculateBoudariesPercent()
        {
            int sumOfBoudaries = 0;
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (PreviousState[i, j].GetGrainNumber() == -2)
                    {
                        sumOfBoudaries += 1;
                    }
                }
            }

            return (double)sumOfBoudaries / (double)(Height * Width) * 100;
        }

        public void FillRandomly()
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (CurrentState[i, j].GetGrainNumber() >= -1)
                    {
                        do
                        {
                            CurrentState[i, j].SetGrainNumber(ColorsMap.ElementAt(GetRandom.Next(0, ColorsMap.Count)).Key);
                        } while (CurrentState[i, j].GetGrainNumber() < -1);
                    }
                }
            }
        }

        public void DistributeEnergy(int energyInsideTheGrains, int energyOnGrainsBorders)
        {
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (CurrentState[i, j].IsCellOnBorder())
                        CurrentState[i, j].SetEnergy(energyOnGrainsBorders);
                    else
                        CurrentState[i, j].SetEnergy(energyInsideTheGrains);
                }
            }
        }

        public void AddRecrystallizedNucleons(int numberOfNucleons, bool nucleonsOnBorders = false)
        {
            for (int i = 0; i < numberOfNucleons; ++i)
            {
                bool placeForNucleonFound = false;
                while (!placeForNucleonFound)
                {
                    int x = GetRandom.Next(0, Height);
                    int y = GetRandom.Next(0, Width);
                    if (!CurrentState[x, y].IsCellRecrystallized() && (!nucleonsOnBorders || CurrentState[x, y].IsCellOnBorder()))
                    {
                        placeForNucleonFound = true;
                        CurrentState[x, y].SetRecrystallized();
                        CurrentState[x, y].SetGrainNumber(ColorsMap.Count);
                        CurrentState[x, y].SetEnergy(0);
                        AddRecrystallizedBrush(ColorsMap.Count);
                    }
                }
            }
            int numberOfRecrystallizedCells = 0;
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (CurrentState[i, j].IsCellRecrystallized())
                        numberOfRecrystallizedCells++;
                }
            }
            Console.WriteLine(numberOfRecrystallizedCells);
            int cellsWithRecrystallizedNeighbors = 0;
            for (int i = 0; i < Height; ++i)
            {
                for (int j = 0; j < Width; ++j)
                {
                    if (CurrentState[i, j].CalculateRecrystallizedNeighbors(CurrentState).Count > 0)
                        cellsWithRecrystallizedNeighbors++;
                }
            }
            Console.WriteLine(cellsWithRecrystallizedNeighbors);
        }
    }
}
