﻿namespace GrainGrowth
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            this.NumberOfGrainsBox = new System.Windows.Forms.TextBox();
            this.NumberOfGrainsLabel = new System.Windows.Forms.Label();
            this.NucleatingButton = new System.Windows.Forms.Button();
            this.GrowthButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.IterationTimer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numberOfInclusionsLabel = new System.Windows.Forms.Label();
            this.NumberOfInclusionsBox = new System.Windows.Forms.TextBox();
            this.InclusionTypeButtonGroup = new System.Windows.Forms.GroupBox();
            this.SquareInclusionButton = new System.Windows.Forms.RadioButton();
            this.CircularInclusionButton = new System.Windows.Forms.RadioButton();
            this.AddInclusionsButton = new System.Windows.Forms.Button();
            this.InclusionDimensionBox = new System.Windows.Forms.TextBox();
            this.InclusionDimensionLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MonteCarloTextBox = new System.Windows.Forms.TextBox();
            this.MonteCarloButton = new System.Windows.Forms.RadioButton();
            this.MooreTextBox = new System.Windows.Forms.TextBox();
            this.MooreButton = new System.Windows.Forms.RadioButton();
            this.VonNeumannButton = new System.Windows.Forms.RadioButton();
            this.GrainsListBox = new System.Windows.Forms.ListBox();
            this.GrainsSelectionButton = new System.Windows.Forms.Button();
            this.StructureTypeBox = new System.Windows.Forms.GroupBox();
            this.DualPhaseButton = new System.Windows.Forms.RadioButton();
            this.SubstructureButton = new System.Windows.Forms.RadioButton();
            this.DrawBoundariesButton = new System.Windows.Forms.Button();
            this.ClearBoundariesButton = new System.Windows.Forms.Button();
            this.BoundariesPercentLabel = new System.Windows.Forms.Label();
            this.MonteCarloIterationTimer = new System.Windows.Forms.Timer(this.components);
            this.EnergyDistributionButtonGroup = new System.Windows.Forms.GroupBox();
            this.HeterogenousDistributionButton = new System.Windows.Forms.RadioButton();
            this.HomogenousDistributionButton = new System.Windows.Forms.RadioButton();
            this.EnergyInsideTextBox = new System.Windows.Forms.TextBox();
            this.EnergyInsideLabel = new System.Windows.Forms.Label();
            this.EnergyOnBordersLabel = new System.Windows.Forms.Label();
            this.EnergyOnBordersTextBox = new System.Windows.Forms.TextBox();
            this.DistributeEnergyButton = new System.Windows.Forms.Button();
            this.EnergyPictureBox = new System.Windows.Forms.PictureBox();
            this.NucleationTypeButtonGroup = new System.Windows.Forms.GroupBox();
            this.InstantNucleationTextBox = new System.Windows.Forms.TextBox();
            this.IncreasingNucleationIncreaseTextBox = new System.Windows.Forms.TextBox();
            this.IncreasingNucleationStartTextBox = new System.Windows.Forms.TextBox();
            this.ConstantNucleationTextBox = new System.Windows.Forms.TextBox();
            this.IncreasingNucleationButton = new System.Windows.Forms.RadioButton();
            this.ConstantNucleationButton = new System.Windows.Forms.RadioButton();
            this.InstantNucleationButton = new System.Windows.Forms.RadioButton();
            this.NucleonsPositionsButtonGroup = new System.Windows.Forms.GroupBox();
            this.NucleonsOnBordersButton = new System.Windows.Forms.RadioButton();
            this.NucleonsAnywhereButton = new System.Windows.Forms.RadioButton();
            this.StartRecrystallizationButton = new System.Windows.Forms.Button();
            this.RecrystallizationTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.InclusionTypeButtonGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.StructureTypeBox.SuspendLayout();
            this.EnergyDistributionButtonGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnergyPictureBox)).BeginInit();
            this.NucleationTypeButtonGroup.SuspendLayout();
            this.NucleonsPositionsButtonGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // PictureBox
            // 
            this.PictureBox.Location = new System.Drawing.Point(209, 52);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(800, 800);
            this.PictureBox.TabIndex = 0;
            this.PictureBox.TabStop = false;
            this.PictureBox.Click += new System.EventHandler(this.PictureBox_Click);
            // 
            // NumberOfGrainsBox
            // 
            this.NumberOfGrainsBox.Location = new System.Drawing.Point(25, 75);
            this.NumberOfGrainsBox.Name = "NumberOfGrainsBox";
            this.NumberOfGrainsBox.Size = new System.Drawing.Size(100, 26);
            this.NumberOfGrainsBox.TabIndex = 1;
            // 
            // NumberOfGrainsLabel
            // 
            this.NumberOfGrainsLabel.AutoSize = true;
            this.NumberOfGrainsLabel.Location = new System.Drawing.Point(21, 52);
            this.NumberOfGrainsLabel.Name = "NumberOfGrainsLabel";
            this.NumberOfGrainsLabel.Size = new System.Drawing.Size(134, 20);
            this.NumberOfGrainsLabel.TabIndex = 2;
            this.NumberOfGrainsLabel.Text = "Number of Grains";
            // 
            // NucleatingButton
            // 
            this.NucleatingButton.Location = new System.Drawing.Point(22, 107);
            this.NucleatingButton.Name = "NucleatingButton";
            this.NucleatingButton.Size = new System.Drawing.Size(100, 41);
            this.NucleatingButton.TabIndex = 3;
            this.NucleatingButton.Text = "Nucleating";
            this.NucleatingButton.UseVisualStyleBackColor = true;
            this.NucleatingButton.Click += new System.EventHandler(this.NucleatingButton_Click);
            // 
            // GrowthButton
            // 
            this.GrowthButton.Enabled = false;
            this.GrowthButton.Location = new System.Drawing.Point(25, 289);
            this.GrowthButton.Name = "GrowthButton";
            this.GrowthButton.Size = new System.Drawing.Size(103, 40);
            this.GrowthButton.TabIndex = 4;
            this.GrowthButton.Text = "Growth";
            this.GrowthButton.UseVisualStyleBackColor = true;
            this.GrowthButton.Click += new System.EventHandler(this.GrowthButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(25, 335);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(103, 39);
            this.ClearButton.TabIndex = 5;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // IterationTimer
            // 
            this.IterationTimer.Tick += new System.EventHandler(this.IterationTimer_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1922, 33);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "MenuStrip";
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportMenuItem,
            this.ExportMenuItem});
            this.FileMenuItem.Name = "FileMenuItem";
            this.FileMenuItem.Size = new System.Drawing.Size(50, 29);
            this.FileMenuItem.Text = "File";
            // 
            // ImportMenuItem
            // 
            this.ImportMenuItem.Name = "ImportMenuItem";
            this.ImportMenuItem.Size = new System.Drawing.Size(151, 30);
            this.ImportMenuItem.Text = "Import";
            this.ImportMenuItem.Click += new System.EventHandler(this.ImportMenuItem_Click);
            // 
            // ExportMenuItem
            // 
            this.ExportMenuItem.Name = "ExportMenuItem";
            this.ExportMenuItem.Size = new System.Drawing.Size(151, 30);
            this.ExportMenuItem.Text = "Export";
            this.ExportMenuItem.Click += new System.EventHandler(this.ExportMenuItem_Click);
            // 
            // numberOfInclusionsLabel
            // 
            this.numberOfInclusionsLabel.AutoSize = true;
            this.numberOfInclusionsLabel.Location = new System.Drawing.Point(22, 420);
            this.numberOfInclusionsLabel.Name = "numberOfInclusionsLabel";
            this.numberOfInclusionsLabel.Size = new System.Drawing.Size(158, 20);
            this.numberOfInclusionsLabel.TabIndex = 7;
            this.numberOfInclusionsLabel.Text = "Number of Inclusions";
            // 
            // NumberOfInclusionsBox
            // 
            this.NumberOfInclusionsBox.Location = new System.Drawing.Point(25, 443);
            this.NumberOfInclusionsBox.Name = "NumberOfInclusionsBox";
            this.NumberOfInclusionsBox.Size = new System.Drawing.Size(100, 26);
            this.NumberOfInclusionsBox.TabIndex = 8;
            // 
            // InclusionTypeButtonGroup
            // 
            this.InclusionTypeButtonGroup.Controls.Add(this.SquareInclusionButton);
            this.InclusionTypeButtonGroup.Controls.Add(this.CircularInclusionButton);
            this.InclusionTypeButtonGroup.Location = new System.Drawing.Point(26, 475);
            this.InclusionTypeButtonGroup.Name = "InclusionTypeButtonGroup";
            this.InclusionTypeButtonGroup.Size = new System.Drawing.Size(168, 100);
            this.InclusionTypeButtonGroup.TabIndex = 9;
            this.InclusionTypeButtonGroup.TabStop = false;
            this.InclusionTypeButtonGroup.Text = "Inclusions Type";
            // 
            // SquareInclusionButton
            // 
            this.SquareInclusionButton.AutoSize = true;
            this.SquareInclusionButton.Checked = true;
            this.SquareInclusionButton.Location = new System.Drawing.Point(30, 60);
            this.SquareInclusionButton.Name = "SquareInclusionButton";
            this.SquareInclusionButton.Size = new System.Drawing.Size(86, 24);
            this.SquareInclusionButton.TabIndex = 1;
            this.SquareInclusionButton.TabStop = true;
            this.SquareInclusionButton.Text = "Square";
            this.SquareInclusionButton.UseVisualStyleBackColor = true;
            // 
            // CircularInclusionButton
            // 
            this.CircularInclusionButton.AutoSize = true;
            this.CircularInclusionButton.Location = new System.Drawing.Point(30, 30);
            this.CircularInclusionButton.Name = "CircularInclusionButton";
            this.CircularInclusionButton.Size = new System.Drawing.Size(87, 24);
            this.CircularInclusionButton.TabIndex = 0;
            this.CircularInclusionButton.Text = "Circular";
            this.CircularInclusionButton.UseVisualStyleBackColor = true;
            // 
            // AddInclusionsButton
            // 
            this.AddInclusionsButton.Location = new System.Drawing.Point(25, 633);
            this.AddInclusionsButton.Name = "AddInclusionsButton";
            this.AddInclusionsButton.Size = new System.Drawing.Size(103, 39);
            this.AddInclusionsButton.TabIndex = 10;
            this.AddInclusionsButton.Text = "Add";
            this.AddInclusionsButton.UseVisualStyleBackColor = true;
            this.AddInclusionsButton.Click += new System.EventHandler(this.AddInclusionsButton_Click);
            // 
            // InclusionDimensionBox
            // 
            this.InclusionDimensionBox.Location = new System.Drawing.Point(25, 601);
            this.InclusionDimensionBox.Name = "InclusionDimensionBox";
            this.InclusionDimensionBox.Size = new System.Drawing.Size(100, 26);
            this.InclusionDimensionBox.TabIndex = 11;
            // 
            // InclusionDimensionLabel
            // 
            this.InclusionDimensionLabel.AutoSize = true;
            this.InclusionDimensionLabel.Location = new System.Drawing.Point(21, 578);
            this.InclusionDimensionLabel.Name = "InclusionDimensionLabel";
            this.InclusionDimensionLabel.Size = new System.Drawing.Size(148, 20);
            this.InclusionDimensionLabel.TabIndex = 12;
            this.InclusionDimensionLabel.Text = "Inclusion dimension";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MonteCarloTextBox);
            this.groupBox1.Controls.Add(this.MonteCarloButton);
            this.groupBox1.Controls.Add(this.MooreTextBox);
            this.groupBox1.Controls.Add(this.MooreButton);
            this.groupBox1.Controls.Add(this.VonNeumannButton);
            this.groupBox1.Location = new System.Drawing.Point(22, 154);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(181, 129);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Growth type";
            // 
            // MonteCarloTextBox
            // 
            this.MonteCarloTextBox.Location = new System.Drawing.Point(123, 99);
            this.MonteCarloTextBox.Name = "MonteCarloTextBox";
            this.MonteCarloTextBox.Size = new System.Drawing.Size(52, 26);
            this.MonteCarloTextBox.TabIndex = 20;
            this.MonteCarloTextBox.Visible = false;
            // 
            // MonteCarloButton
            // 
            this.MonteCarloButton.AutoSize = true;
            this.MonteCarloButton.Location = new System.Drawing.Point(6, 99);
            this.MonteCarloButton.Name = "MonteCarloButton";
            this.MonteCarloButton.Size = new System.Drawing.Size(120, 24);
            this.MonteCarloButton.TabIndex = 20;
            this.MonteCarloButton.TabStop = true;
            this.MonteCarloButton.Text = "Monte Carlo";
            this.MonteCarloButton.UseVisualStyleBackColor = true;
            this.MonteCarloButton.CheckedChanged += new System.EventHandler(this.MonteCarloButton_CheckedChanged);
            // 
            // MooreTextBox
            // 
            this.MooreTextBox.Location = new System.Drawing.Point(87, 58);
            this.MooreTextBox.Name = "MooreTextBox";
            this.MooreTextBox.Size = new System.Drawing.Size(71, 26);
            this.MooreTextBox.TabIndex = 14;
            this.MooreTextBox.Visible = false;
            // 
            // MooreButton
            // 
            this.MooreButton.AutoSize = true;
            this.MooreButton.Location = new System.Drawing.Point(6, 60);
            this.MooreButton.Name = "MooreButton";
            this.MooreButton.Size = new System.Drawing.Size(79, 24);
            this.MooreButton.TabIndex = 14;
            this.MooreButton.Text = "Moore";
            this.MooreButton.UseVisualStyleBackColor = true;
            this.MooreButton.CheckedChanged += new System.EventHandler(this.MooreButton_CheckedChanged);
            // 
            // VonNeumannButton
            // 
            this.VonNeumannButton.AutoSize = true;
            this.VonNeumannButton.Checked = true;
            this.VonNeumannButton.Location = new System.Drawing.Point(6, 25);
            this.VonNeumannButton.Name = "VonNeumannButton";
            this.VonNeumannButton.Size = new System.Drawing.Size(136, 24);
            this.VonNeumannButton.TabIndex = 14;
            this.VonNeumannButton.TabStop = true;
            this.VonNeumannButton.Text = "Von Neumann";
            this.VonNeumannButton.UseVisualStyleBackColor = true;
            this.VonNeumannButton.CheckedChanged += new System.EventHandler(this.VonNeumannButton_CheckedChanged);
            // 
            // GrainsListBox
            // 
            this.GrainsListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.GrainsListBox.FormattingEnabled = true;
            this.GrainsListBox.ItemHeight = 20;
            this.GrainsListBox.Location = new System.Drawing.Point(1162, 52);
            this.GrainsListBox.Name = "GrainsListBox";
            this.GrainsListBox.Size = new System.Drawing.Size(238, 304);
            this.GrainsListBox.TabIndex = 14;
            this.GrainsListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.GrainsListBox_DrawItem);
            this.GrainsListBox.SelectedIndexChanged += new System.EventHandler(this.GrainsListBox_SelectedIndexChanged);
            // 
            // GrainsSelectionButton
            // 
            this.GrainsSelectionButton.Location = new System.Drawing.Point(1163, 456);
            this.GrainsSelectionButton.Name = "GrainsSelectionButton";
            this.GrainsSelectionButton.Size = new System.Drawing.Size(132, 49);
            this.GrainsSelectionButton.TabIndex = 15;
            this.GrainsSelectionButton.Text = "Select Grains";
            this.GrainsSelectionButton.UseVisualStyleBackColor = true;
            this.GrainsSelectionButton.Click += new System.EventHandler(this.GrainsSelectionButton_Click);
            // 
            // StructureTypeBox
            // 
            this.StructureTypeBox.Controls.Add(this.DualPhaseButton);
            this.StructureTypeBox.Controls.Add(this.SubstructureButton);
            this.StructureTypeBox.Location = new System.Drawing.Point(1162, 362);
            this.StructureTypeBox.Name = "StructureTypeBox";
            this.StructureTypeBox.Size = new System.Drawing.Size(238, 88);
            this.StructureTypeBox.TabIndex = 16;
            this.StructureTypeBox.TabStop = false;
            this.StructureTypeBox.Text = "Structure type";
            // 
            // DualPhaseButton
            // 
            this.DualPhaseButton.AutoSize = true;
            this.DualPhaseButton.Location = new System.Drawing.Point(7, 58);
            this.DualPhaseButton.Name = "DualPhaseButton";
            this.DualPhaseButton.Size = new System.Drawing.Size(115, 24);
            this.DualPhaseButton.TabIndex = 1;
            this.DualPhaseButton.Text = "Dual phase";
            this.DualPhaseButton.UseVisualStyleBackColor = true;
            // 
            // SubstructureButton
            // 
            this.SubstructureButton.AutoSize = true;
            this.SubstructureButton.Checked = true;
            this.SubstructureButton.Location = new System.Drawing.Point(7, 26);
            this.SubstructureButton.Name = "SubstructureButton";
            this.SubstructureButton.Size = new System.Drawing.Size(126, 24);
            this.SubstructureButton.TabIndex = 0;
            this.SubstructureButton.TabStop = true;
            this.SubstructureButton.Text = "Substructure";
            this.SubstructureButton.UseVisualStyleBackColor = true;
            // 
            // DrawBoundariesButton
            // 
            this.DrawBoundariesButton.Location = new System.Drawing.Point(1163, 523);
            this.DrawBoundariesButton.Name = "DrawBoundariesButton";
            this.DrawBoundariesButton.Size = new System.Drawing.Size(174, 47);
            this.DrawBoundariesButton.TabIndex = 17;
            this.DrawBoundariesButton.Text = "Draw boundaries";
            this.DrawBoundariesButton.UseVisualStyleBackColor = true;
            this.DrawBoundariesButton.Click += new System.EventHandler(this.DrawBoundariesButton_Click);
            // 
            // ClearBoundariesButton
            // 
            this.ClearBoundariesButton.Location = new System.Drawing.Point(1163, 576);
            this.ClearBoundariesButton.Name = "ClearBoundariesButton";
            this.ClearBoundariesButton.Size = new System.Drawing.Size(174, 44);
            this.ClearBoundariesButton.TabIndex = 18;
            this.ClearBoundariesButton.Text = "Clear boundaries";
            this.ClearBoundariesButton.UseVisualStyleBackColor = true;
            this.ClearBoundariesButton.Click += new System.EventHandler(this.ClearBoundariesButton_Click);
            // 
            // BoundariesPercentLabel
            // 
            this.BoundariesPercentLabel.AutoSize = true;
            this.BoundariesPercentLabel.Location = new System.Drawing.Point(1343, 536);
            this.BoundariesPercentLabel.Name = "BoundariesPercentLabel";
            this.BoundariesPercentLabel.Size = new System.Drawing.Size(51, 20);
            this.BoundariesPercentLabel.TabIndex = 19;
            this.BoundariesPercentLabel.Text = "GB %";
            this.BoundariesPercentLabel.Visible = false;
            // 
            // MonteCarloIterationTimer
            // 
            this.MonteCarloIterationTimer.Tick += new System.EventHandler(this.MonteCarloIterationTimer_Tick);
            // 
            // EnergyDistributionButtonGroup
            // 
            this.EnergyDistributionButtonGroup.Controls.Add(this.HeterogenousDistributionButton);
            this.EnergyDistributionButtonGroup.Controls.Add(this.HomogenousDistributionButton);
            this.EnergyDistributionButtonGroup.Location = new System.Drawing.Point(1532, 459);
            this.EnergyDistributionButtonGroup.Name = "EnergyDistributionButtonGroup";
            this.EnergyDistributionButtonGroup.Size = new System.Drawing.Size(182, 100);
            this.EnergyDistributionButtonGroup.TabIndex = 20;
            this.EnergyDistributionButtonGroup.TabStop = false;
            this.EnergyDistributionButtonGroup.Text = "Energy Distribution";
            // 
            // HeterogenousDistributionButton
            // 
            this.HeterogenousDistributionButton.AutoSize = true;
            this.HeterogenousDistributionButton.Checked = true;
            this.HeterogenousDistributionButton.Location = new System.Drawing.Point(28, 60);
            this.HeterogenousDistributionButton.Name = "HeterogenousDistributionButton";
            this.HeterogenousDistributionButton.Size = new System.Drawing.Size(136, 24);
            this.HeterogenousDistributionButton.TabIndex = 1;
            this.HeterogenousDistributionButton.TabStop = true;
            this.HeterogenousDistributionButton.Text = "Heterogenous";
            this.HeterogenousDistributionButton.UseVisualStyleBackColor = true;
            this.HeterogenousDistributionButton.CheckedChanged += new System.EventHandler(this.HeterogenousDistributionButton_CheckedChanged);
            // 
            // HomogenousDistributionButton
            // 
            this.HomogenousDistributionButton.AutoSize = true;
            this.HomogenousDistributionButton.Location = new System.Drawing.Point(28, 30);
            this.HomogenousDistributionButton.Name = "HomogenousDistributionButton";
            this.HomogenousDistributionButton.Size = new System.Drawing.Size(130, 24);
            this.HomogenousDistributionButton.TabIndex = 0;
            this.HomogenousDistributionButton.Text = "Homogenous";
            this.HomogenousDistributionButton.UseVisualStyleBackColor = true;
            this.HomogenousDistributionButton.CheckedChanged += new System.EventHandler(this.HomogenousDistributionButton_CheckedChanged);
            // 
            // EnergyInsideTextBox
            // 
            this.EnergyInsideTextBox.Location = new System.Drawing.Point(1673, 562);
            this.EnergyInsideTextBox.Name = "EnergyInsideTextBox";
            this.EnergyInsideTextBox.Size = new System.Drawing.Size(41, 26);
            this.EnergyInsideTextBox.TabIndex = 21;
            // 
            // EnergyInsideLabel
            // 
            this.EnergyInsideLabel.AutoSize = true;
            this.EnergyInsideLabel.Location = new System.Drawing.Point(1531, 565);
            this.EnergyInsideLabel.Name = "EnergyInsideLabel";
            this.EnergyInsideLabel.Size = new System.Drawing.Size(106, 20);
            this.EnergyInsideLabel.TabIndex = 22;
            this.EnergyInsideLabel.Text = "Energy Inside";
            // 
            // EnergyOnBordersLabel
            // 
            this.EnergyOnBordersLabel.AutoSize = true;
            this.EnergyOnBordersLabel.Location = new System.Drawing.Point(1531, 600);
            this.EnergyOnBordersLabel.Name = "EnergyOnBordersLabel";
            this.EnergyOnBordersLabel.Size = new System.Drawing.Size(141, 20);
            this.EnergyOnBordersLabel.TabIndex = 23;
            this.EnergyOnBordersLabel.Text = "Energy on Borders";
            // 
            // EnergyOnBordersTextBox
            // 
            this.EnergyOnBordersTextBox.Location = new System.Drawing.Point(1673, 597);
            this.EnergyOnBordersTextBox.Name = "EnergyOnBordersTextBox";
            this.EnergyOnBordersTextBox.Size = new System.Drawing.Size(41, 26);
            this.EnergyOnBordersTextBox.TabIndex = 24;
            // 
            // DistributeEnergyButton
            // 
            this.DistributeEnergyButton.Location = new System.Drawing.Point(1543, 631);
            this.DistributeEnergyButton.Name = "DistributeEnergyButton";
            this.DistributeEnergyButton.Size = new System.Drawing.Size(153, 43);
            this.DistributeEnergyButton.TabIndex = 25;
            this.DistributeEnergyButton.Text = "Distribute energy";
            this.DistributeEnergyButton.UseVisualStyleBackColor = true;
            this.DistributeEnergyButton.Click += new System.EventHandler(this.DistributeEnergyButton_Click);
            // 
            // EnergyPictureBox
            // 
            this.EnergyPictureBox.Location = new System.Drawing.Point(1415, 52);
            this.EnergyPictureBox.Name = "EnergyPictureBox";
            this.EnergyPictureBox.Size = new System.Drawing.Size(400, 400);
            this.EnergyPictureBox.TabIndex = 26;
            this.EnergyPictureBox.TabStop = false;
            this.EnergyPictureBox.Visible = false;
            // 
            // NucleationTypeButtonGroup
            // 
            this.NucleationTypeButtonGroup.Controls.Add(this.InstantNucleationTextBox);
            this.NucleationTypeButtonGroup.Controls.Add(this.IncreasingNucleationIncreaseTextBox);
            this.NucleationTypeButtonGroup.Controls.Add(this.IncreasingNucleationStartTextBox);
            this.NucleationTypeButtonGroup.Controls.Add(this.ConstantNucleationTextBox);
            this.NucleationTypeButtonGroup.Controls.Add(this.IncreasingNucleationButton);
            this.NucleationTypeButtonGroup.Controls.Add(this.ConstantNucleationButton);
            this.NucleationTypeButtonGroup.Controls.Add(this.InstantNucleationButton);
            this.NucleationTypeButtonGroup.Location = new System.Drawing.Point(1428, 684);
            this.NucleationTypeButtonGroup.Name = "NucleationTypeButtonGroup";
            this.NucleationTypeButtonGroup.Size = new System.Drawing.Size(257, 128);
            this.NucleationTypeButtonGroup.TabIndex = 27;
            this.NucleationTypeButtonGroup.TabStop = false;
            this.NucleationTypeButtonGroup.Text = "Nucleation type";
            // 
            // InstantNucleationTextBox
            // 
            this.InstantNucleationTextBox.Location = new System.Drawing.Point(138, 26);
            this.InstantNucleationTextBox.Name = "InstantNucleationTextBox";
            this.InstantNucleationTextBox.Size = new System.Drawing.Size(47, 26);
            this.InstantNucleationTextBox.TabIndex = 29;
            // 
            // IncreasingNucleationIncreaseTextBox
            // 
            this.IncreasingNucleationIncreaseTextBox.Location = new System.Drawing.Point(191, 86);
            this.IncreasingNucleationIncreaseTextBox.Name = "IncreasingNucleationIncreaseTextBox";
            this.IncreasingNucleationIncreaseTextBox.Size = new System.Drawing.Size(47, 26);
            this.IncreasingNucleationIncreaseTextBox.TabIndex = 28;
            this.IncreasingNucleationIncreaseTextBox.Visible = false;
            // 
            // IncreasingNucleationStartTextBox
            // 
            this.IncreasingNucleationStartTextBox.Location = new System.Drawing.Point(138, 86);
            this.IncreasingNucleationStartTextBox.Name = "IncreasingNucleationStartTextBox";
            this.IncreasingNucleationStartTextBox.Size = new System.Drawing.Size(47, 26);
            this.IncreasingNucleationStartTextBox.TabIndex = 4;
            this.IncreasingNucleationStartTextBox.Visible = false;
            // 
            // ConstantNucleationTextBox
            // 
            this.ConstantNucleationTextBox.Location = new System.Drawing.Point(138, 56);
            this.ConstantNucleationTextBox.Name = "ConstantNucleationTextBox";
            this.ConstantNucleationTextBox.Size = new System.Drawing.Size(47, 26);
            this.ConstantNucleationTextBox.TabIndex = 3;
            this.ConstantNucleationTextBox.Visible = false;
            // 
            // IncreasingNucleationButton
            // 
            this.IncreasingNucleationButton.AutoSize = true;
            this.IncreasingNucleationButton.Location = new System.Drawing.Point(24, 86);
            this.IncreasingNucleationButton.Name = "IncreasingNucleationButton";
            this.IncreasingNucleationButton.Size = new System.Drawing.Size(108, 24);
            this.IncreasingNucleationButton.TabIndex = 2;
            this.IncreasingNucleationButton.Text = "Increasing";
            this.IncreasingNucleationButton.UseVisualStyleBackColor = true;
            this.IncreasingNucleationButton.CheckedChanged += new System.EventHandler(this.IncreasingNucleationButton_CheckedChanged);
            // 
            // ConstantNucleationButton
            // 
            this.ConstantNucleationButton.AutoSize = true;
            this.ConstantNucleationButton.Location = new System.Drawing.Point(24, 56);
            this.ConstantNucleationButton.Name = "ConstantNucleationButton";
            this.ConstantNucleationButton.Size = new System.Drawing.Size(99, 24);
            this.ConstantNucleationButton.TabIndex = 1;
            this.ConstantNucleationButton.Text = "Constant";
            this.ConstantNucleationButton.UseVisualStyleBackColor = true;
            this.ConstantNucleationButton.CheckedChanged += new System.EventHandler(this.ConstantNucleationButton_CheckedChanged);
            // 
            // InstantNucleationButton
            // 
            this.InstantNucleationButton.AutoSize = true;
            this.InstantNucleationButton.Checked = true;
            this.InstantNucleationButton.Location = new System.Drawing.Point(24, 26);
            this.InstantNucleationButton.Name = "InstantNucleationButton";
            this.InstantNucleationButton.Size = new System.Drawing.Size(84, 24);
            this.InstantNucleationButton.TabIndex = 0;
            this.InstantNucleationButton.TabStop = true;
            this.InstantNucleationButton.Text = "Instant";
            this.InstantNucleationButton.UseVisualStyleBackColor = true;
            this.InstantNucleationButton.CheckedChanged += new System.EventHandler(this.InstantNucleationButton_CheckedChanged);
            // 
            // NucleonsPositionsButtonGroup
            // 
            this.NucleonsPositionsButtonGroup.Controls.Add(this.NucleonsOnBordersButton);
            this.NucleonsPositionsButtonGroup.Controls.Add(this.NucleonsAnywhereButton);
            this.NucleonsPositionsButtonGroup.Location = new System.Drawing.Point(1691, 684);
            this.NucleonsPositionsButtonGroup.Name = "NucleonsPositionsButtonGroup";
            this.NucleonsPositionsButtonGroup.Size = new System.Drawing.Size(158, 128);
            this.NucleonsPositionsButtonGroup.TabIndex = 28;
            this.NucleonsPositionsButtonGroup.TabStop = false;
            this.NucleonsPositionsButtonGroup.Text = "Nucleons positions";
            // 
            // NucleonsOnBordersButton
            // 
            this.NucleonsOnBordersButton.AutoSize = true;
            this.NucleonsOnBordersButton.Location = new System.Drawing.Point(24, 76);
            this.NucleonsOnBordersButton.Name = "NucleonsOnBordersButton";
            this.NucleonsOnBordersButton.Size = new System.Drawing.Size(113, 24);
            this.NucleonsOnBordersButton.TabIndex = 1;
            this.NucleonsOnBordersButton.Text = "On borders";
            this.NucleonsOnBordersButton.UseVisualStyleBackColor = true;
            // 
            // NucleonsAnywhereButton
            // 
            this.NucleonsAnywhereButton.AutoSize = true;
            this.NucleonsAnywhereButton.Checked = true;
            this.NucleonsAnywhereButton.Location = new System.Drawing.Point(24, 46);
            this.NucleonsAnywhereButton.Name = "NucleonsAnywhereButton";
            this.NucleonsAnywhereButton.Size = new System.Drawing.Size(104, 24);
            this.NucleonsAnywhereButton.TabIndex = 0;
            this.NucleonsAnywhereButton.TabStop = true;
            this.NucleonsAnywhereButton.Text = "Anywhere";
            this.NucleonsAnywhereButton.UseVisualStyleBackColor = true;
            // 
            // StartRecrystallizationButton
            // 
            this.StartRecrystallizationButton.Location = new System.Drawing.Point(1527, 818);
            this.StartRecrystallizationButton.Name = "StartRecrystallizationButton";
            this.StartRecrystallizationButton.Size = new System.Drawing.Size(187, 50);
            this.StartRecrystallizationButton.TabIndex = 29;
            this.StartRecrystallizationButton.Text = "Start recrystallization";
            this.StartRecrystallizationButton.UseVisualStyleBackColor = true;
            this.StartRecrystallizationButton.Click += new System.EventHandler(this.StartRecrystallizationButton_Click);
            // 
            // RecrystallizationTimer
            // 
            this.RecrystallizationTimer.Tick += new System.EventHandler(this.RecrystallizationTimer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1922, 1050);
            this.Controls.Add(this.StartRecrystallizationButton);
            this.Controls.Add(this.NucleonsPositionsButtonGroup);
            this.Controls.Add(this.NucleationTypeButtonGroup);
            this.Controls.Add(this.EnergyPictureBox);
            this.Controls.Add(this.DistributeEnergyButton);
            this.Controls.Add(this.EnergyOnBordersTextBox);
            this.Controls.Add(this.EnergyOnBordersLabel);
            this.Controls.Add(this.EnergyInsideLabel);
            this.Controls.Add(this.EnergyInsideTextBox);
            this.Controls.Add(this.EnergyDistributionButtonGroup);
            this.Controls.Add(this.BoundariesPercentLabel);
            this.Controls.Add(this.ClearBoundariesButton);
            this.Controls.Add(this.DrawBoundariesButton);
            this.Controls.Add(this.StructureTypeBox);
            this.Controls.Add(this.GrainsSelectionButton);
            this.Controls.Add(this.GrainsListBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.InclusionDimensionLabel);
            this.Controls.Add(this.InclusionDimensionBox);
            this.Controls.Add(this.AddInclusionsButton);
            this.Controls.Add(this.InclusionTypeButtonGroup);
            this.Controls.Add(this.NumberOfInclusionsBox);
            this.Controls.Add(this.numberOfInclusionsLabel);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.GrowthButton);
            this.Controls.Add(this.NucleatingButton);
            this.Controls.Add(this.NumberOfGrainsLabel);
            this.Controls.Add(this.NumberOfGrainsBox);
            this.Controls.Add(this.PictureBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1139, 1078);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.InclusionTypeButtonGroup.ResumeLayout(false);
            this.InclusionTypeButtonGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.StructureTypeBox.ResumeLayout(false);
            this.StructureTypeBox.PerformLayout();
            this.EnergyDistributionButtonGroup.ResumeLayout(false);
            this.EnergyDistributionButtonGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EnergyPictureBox)).EndInit();
            this.NucleationTypeButtonGroup.ResumeLayout(false);
            this.NucleationTypeButtonGroup.PerformLayout();
            this.NucleonsPositionsButtonGroup.ResumeLayout(false);
            this.NucleonsPositionsButtonGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.TextBox NumberOfGrainsBox;
        private System.Windows.Forms.Label NumberOfGrainsLabel;
        private System.Windows.Forms.Button NucleatingButton;
        private System.Windows.Forms.Button GrowthButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Timer IterationTimer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExportMenuItem;
        private System.Windows.Forms.Label numberOfInclusionsLabel;
        private System.Windows.Forms.TextBox NumberOfInclusionsBox;
        private System.Windows.Forms.GroupBox InclusionTypeButtonGroup;
        private System.Windows.Forms.RadioButton SquareInclusionButton;
        private System.Windows.Forms.RadioButton CircularInclusionButton;
        private System.Windows.Forms.Button AddInclusionsButton;
        private System.Windows.Forms.TextBox InclusionDimensionBox;
        private System.Windows.Forms.Label InclusionDimensionLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox MooreTextBox;
        private System.Windows.Forms.RadioButton MooreButton;
        private System.Windows.Forms.RadioButton VonNeumannButton;
        private System.Windows.Forms.ListBox GrainsListBox;
        private System.Windows.Forms.Button GrainsSelectionButton;
        private System.Windows.Forms.GroupBox StructureTypeBox;
        private System.Windows.Forms.RadioButton DualPhaseButton;
        private System.Windows.Forms.RadioButton SubstructureButton;
        private System.Windows.Forms.Button DrawBoundariesButton;
        private System.Windows.Forms.Button ClearBoundariesButton;
        private System.Windows.Forms.Label BoundariesPercentLabel;
        private System.Windows.Forms.RadioButton MonteCarloButton;
        private System.Windows.Forms.Timer MonteCarloIterationTimer;
        private System.Windows.Forms.TextBox MonteCarloTextBox;
        private System.Windows.Forms.GroupBox EnergyDistributionButtonGroup;
        private System.Windows.Forms.RadioButton HeterogenousDistributionButton;
        private System.Windows.Forms.RadioButton HomogenousDistributionButton;
        private System.Windows.Forms.TextBox EnergyInsideTextBox;
        private System.Windows.Forms.Label EnergyInsideLabel;
        private System.Windows.Forms.Label EnergyOnBordersLabel;
        private System.Windows.Forms.TextBox EnergyOnBordersTextBox;
        private System.Windows.Forms.Button DistributeEnergyButton;
        private System.Windows.Forms.PictureBox EnergyPictureBox;
        private System.Windows.Forms.GroupBox NucleationTypeButtonGroup;
        private System.Windows.Forms.TextBox IncreasingNucleationStartTextBox;
        private System.Windows.Forms.TextBox ConstantNucleationTextBox;
        private System.Windows.Forms.RadioButton IncreasingNucleationButton;
        private System.Windows.Forms.RadioButton ConstantNucleationButton;
        private System.Windows.Forms.RadioButton InstantNucleationButton;
        private System.Windows.Forms.TextBox IncreasingNucleationIncreaseTextBox;
        private System.Windows.Forms.GroupBox NucleonsPositionsButtonGroup;
        private System.Windows.Forms.RadioButton NucleonsOnBordersButton;
        private System.Windows.Forms.RadioButton NucleonsAnywhereButton;
        private System.Windows.Forms.TextBox InstantNucleationTextBox;
        private System.Windows.Forms.Button StartRecrystallizationButton;
        private System.Windows.Forms.Timer RecrystallizationTimer;
    }
}

