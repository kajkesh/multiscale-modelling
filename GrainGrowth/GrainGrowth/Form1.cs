﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace GrainGrowth
{
    public partial class Form1 : Form
    {
        CASpace caSpace;
        int MonteCarloIterations;
        int NumberOfRecrystallizedNucleons;
        int IncreaseOfRecrystallizedNucleons;
        bool NucleonsOnBorders;
        public Form1()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            int width = 300;
            int height = 300;
            caSpace = new CASpace(width, height);
            SetNeighborhood();
            PictureBox.Width = height * 2;
            PictureBox.Height = width * 2;
        }

        private void NucleatingButton_Click(object sender, EventArgs e)
        {
            NumberOfInclusionsBox.Enabled = false;
            InclusionTypeButtonGroup.Enabled = false;
            AddInclusionsButton.Enabled = false;
            Int32.TryParse(NumberOfGrainsBox.Text, out int numberOfNucleons);

            for (int i = 0; i < numberOfNucleons; ++i)
            {
                caSpace.CreateNewNucleon();
            }
            DrawTheSpace();
            GrowthButton.Enabled = true;
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            GrowthButton.Enabled = false;
            caSpace.ZeroTheSpace();
            DrawTheSpace();
            BoundariesPercentLabel.Visible = false;
            EnergyPictureBox.Visible = false;
        }

        private void GrowthButton_Click(object sender, EventArgs e)
        {
            DisableButtons();
            if (VonNeumannButton.Checked)
            {
                caSpace.SetNeighborhood(NeighborhoodType.VonNeumann);
                IterationTimer.Enabled = true;
            }
            else if (MooreButton.Checked)
            {
                Int32.TryParse(MooreTextBox.Text, out int probability);
                caSpace.SetProbability(probability);
                caSpace.SetNeighborhood(NeighborhoodType.Moore);
                IterationTimer.Enabled = true;
            }
            else
            {
                Int32.TryParse(MonteCarloTextBox.Text, out MonteCarloIterations);
                caSpace.SetNeighborhood(NeighborhoodType.Moore);
                caSpace.FillRandomly();
                DrawTheSpace();
                MonteCarloIterationTimer.Enabled = true;
            }
        }

        private void DisableButtons()
        {
            NucleatingButton.Enabled = false;
            GrowthButton.Enabled = false;
            ClearButton.Enabled = false;
            NumberOfGrainsBox.Enabled = false;
            NumberOfInclusionsBox.Enabled = false;
            InclusionTypeButtonGroup.Enabled = false;
            AddInclusionsButton.Enabled = false;
        }

        private void EnableButtons()
        {
            NucleatingButton.Enabled = true;
            GrowthButton.Enabled = true;
            ClearButton.Enabled = true;
            NumberOfGrainsBox.Enabled = true;
            NumberOfInclusionsBox.Enabled = true;
            InclusionTypeButtonGroup.Enabled = true;
            AddInclusionsButton.Enabled = true;
        }

        private void DrawTheSpace()
        {
            PictureBox.Image = caSpace.DrawTheSpace();
        }

        private void DrawTheEnergy()
        {
            EnergyPictureBox.Image = caSpace.DrawTheEnergy();
        }

        private void SetNeighborhood()
        {
            caSpace.SetNeighborhood();
        }

        private void IterationTimer_Tick(object sender, EventArgs e)
        {
            if (!caSpace.IsFull() && caSpace.Iteration())
            {
                DrawTheSpace();
            }
            else
            {
                IterationTimer.Enabled = false;
                EnableButtons();
            }
        }

        private void ImportMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                Filter = "Bitmap Image|*.bmp|Text file|*.txt",
                Title = "Open microstructure"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog.FileName != "")
                {
                    switch (openFileDialog.FilterIndex)
                    {
                        case 1:
                            Bitmap bitmap = new Bitmap(openFileDialog.FileName);
                            caSpace = new CASpace(bitmap.Height / 2, bitmap.Width / 2);
                            SetNeighborhood();
                            PictureBox.Width = bitmap.Width;
                            PictureBox.Height = bitmap.Height;
                            for (int i = 0; i < bitmap.Height; i += 2)
                            {
                                for (int j = 0; j < bitmap.Width; j += 2)
                                {
                                    Color pixel = bitmap.GetPixel(i, j);
                                    int grainNumber;
                                    if (pixel.R == 255 && pixel.G == 255 && pixel.B == 255)
                                    {
                                        grainNumber = -1;
                                    }
                                    else if (pixel.R == 0 && pixel.G == 0 && pixel.B == 0)
                                    {
                                        grainNumber = -2;
                                    }
                                    else
                                    {
                                        grainNumber = caSpace.GetGrainNumberForColor(pixel);
                                        if (grainNumber == -1)
                                        {
                                            grainNumber = caSpace.AddNewBrush(pixel);
                                        }
                                    }
                                    caSpace.CurrentState[j / 2, i / 2].SetGrainNumber(grainNumber);
                                    caSpace.PreviousState[j / 2, i / 2].SetGrainNumber(grainNumber);
                                }
                            }
                            Console.WriteLine(caSpace.ColorsMap.Count);
                            DrawTheSpace();
                            Console.WriteLine("Opened .bmp file");
                            break;

                        case 2:
                            StreamReader streamReader = new StreamReader(openFileDialog.FileName);
                            String[] lines = streamReader.ReadToEnd().Split('\n');
                            Console.WriteLine(lines[1]);
                            String[] words = lines[0].Split(' ');
                            caSpace = new CASpace(Int32.Parse(words[0]), Int32.Parse(words[1]));
                            SetNeighborhood();
                            PictureBox.Width = Int32.Parse(words[1]) * 2;
                            PictureBox.Height = Int32.Parse(words[0]) * 2;
                            for (int i = 2; i < lines.Length - 1; ++i)
                            {
                                String[] elements = lines[i].Split(' ');
                                int grainNumber = Int32.Parse(elements[0]);
                                int row = Int32.Parse(elements[1]);
                                int column = Int32.Parse(elements[2]);
                                caSpace.CurrentState[row, column].SetGrainNumber(grainNumber);
                                caSpace.PreviousState[row, column].SetGrainNumber(grainNumber);
                                if (!caSpace.IsColorForGrain(grainNumber))
                                    caSpace.AddNewBrush(grainNumber);
                            }
                            streamReader.Close();
                            DrawTheSpace();
                            Console.WriteLine("Opened .txt file");
                            break;
                    }
                }
            }
        }

        private void ExportMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "Bitmap Image|*.bmp|Text file|*.txt",
                Title = "Save microstructure"
            };
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog.FileName != "")
                {
                    switch (saveFileDialog.FilterIndex)
                    {
                        case 1:
                            Bitmap bitmap = caSpace.DrawTheSpace();
                            bitmap.Save(saveFileDialog.FileName);
                            Console.WriteLine(bitmap.Width);
                            Console.WriteLine("Saved .bmp file");
                            break;
                        case 2:
                            FileStream fileStream = (FileStream)saveFileDialog.OpenFile();
                            StreamWriter fileWriter = new StreamWriter(fileStream);
                            fileWriter.WriteLine(caSpace.GetWidth() + " " + caSpace.GetHeight());
                            for (int i = 0; i < caSpace.GetHeight(); ++i)
                            {
                                for (int j = 0; j < caSpace.GetWidth(); ++j)
                                {
                                    fileWriter.WriteLine(caSpace.CurrentState[i, j].GetGrainNumber()
                                        + " " + caSpace.CurrentState[i, j].GetRow()
                                        + " " + caSpace.CurrentState[i, j].GetColumn());
                                }
                            }
                            fileWriter.Flush();
                            fileWriter.Close();
                            fileStream.Close();
                            Console.WriteLine("Saved .txt file");
                            break;
                    }
                }
            }
        }

        private void AddInclusionsButton_Click(object sender, EventArgs e)
        {
            Int32.TryParse(NumberOfInclusionsBox.Text, out int numberOfInclusions);
            Int32.TryParse(InclusionDimensionBox.Text, out int inclusionDimension);
            bool areInclusionsSquare = true;
            inclusionDimension = areInclusionsSquare ? inclusionDimension / 2 : inclusionDimension;
            if (CircularInclusionButton.Checked)
            {
                areInclusionsSquare = false;
            }
            if (caSpace.IsFull())
            {
                caSpace.SetTheBorders();
                for (int i = 0; i < numberOfInclusions; ++i)
                {
                    caSpace.CreateNewInclusion(inclusionDimension, areInclusionsSquare, true);
                }
                DrawTheSpace();
            }
            else
            {
                for (int i = 0; i < numberOfInclusions; ++i)
                {
                    caSpace.CreateNewInclusion(inclusionDimension, areInclusionsSquare);
                }
                DrawTheSpace();
            }
        }

        private void MooreButton_CheckedChanged(object sender, EventArgs e)
        {
            MooreTextBox.Visible = true;
            MonteCarloTextBox.Visible = false;
        }

        private void VonNeumannButton_CheckedChanged(object sender, EventArgs e)
        {
            MooreTextBox.Visible = false;
            MonteCarloTextBox.Visible = false;
        }

        private void PictureBox_Click(object sender, EventArgs e)
        {
            MouseEventArgs mouseEvent = (MouseEventArgs)e;
            Point coordinates = mouseEvent.Location;
            int i = coordinates.Y / 2;
            int j = coordinates.X / 2;
            int grainNumber = caSpace.PreviousState[i, j].GetGrainNumber();

            if (!GrainsListBox.Items.Contains(grainNumber.ToString()))
                GrainsListBox.Items.Add(grainNumber.ToString());
        }

        private void GrainsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            GrainsListBox.Items.Remove(GrainsListBox.SelectedItem);
        }

        private void GrainsSelectionButton_Click(object sender, EventArgs e)
        {
            if (SubstructureButton.Checked)
            {
                caSpace.SelectTheGrains(GrainsListBox.Items);
            }
            else
            {
                caSpace.SelectTheGrains(GrainsListBox.Items, true);
            }
            GrainsListBox.Items.Clear();
            DrawTheSpace();
            GrowthButton.Enabled = false;
        }

        private void DrawBoundariesButton_Click(object sender, EventArgs e)
        {
            caSpace.SetTheBorders();
            caSpace.DrawTheBorders(GrainsListBox.Items);
            GrainsListBox.Items.Clear();
            DrawTheSpace();
            BoundariesPercentLabel.Text = "GB " + Math.Round(caSpace.CalculateBoudariesPercent(), 2) + " %";
            BoundariesPercentLabel.Visible = true;
        }

        private void ClearBoundariesButton_Click(object sender, EventArgs e)
        {
            caSpace.ClearTheBorders();
            GrowthButton.Enabled = false;
            DrawTheSpace();
        }

        private void GrainsListBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();

            SolidBrush myBrush = caSpace.ColorsMap[Convert.ToInt32(GrainsListBox.Items[e.Index])];

            e.Graphics.FillRectangle(myBrush, e.Bounds);
            e.Graphics.DrawString(GrainsListBox.Items[e.Index].ToString(),
                e.Font, new SolidBrush(e.ForeColor), e.Bounds, StringFormat.GenericDefault);

            e.DrawFocusRectangle();
        }

        private void MonteCarloIterationTimer_Tick(object sender, EventArgs e)
        {
            if (MonteCarloIterations > 0)
            {
                caSpace.MonteCarloIteration();
                DrawTheSpace();
                --MonteCarloIterations;
            }
            else
            {
                MonteCarloIterationTimer.Enabled = false;
                caSpace.RewriteStates();
                EnableButtons();
            }
        }

        private void MonteCarloButton_CheckedChanged(object sender, EventArgs e)
        {
            MonteCarloTextBox.Visible = true;
            MooreTextBox.Visible = false;
        }

        private void HeterogenousDistributionButton_CheckedChanged(object sender, EventArgs e)
        {
            EnergyOnBordersLabel.Visible = true;
            EnergyOnBordersTextBox.Visible = true;
        }

        private void HomogenousDistributionButton_CheckedChanged(object sender, EventArgs e)
        {
            EnergyOnBordersLabel.Visible = false;
            EnergyOnBordersTextBox.Visible = false;
        }

        private void DistributeEnergyButton_Click(object sender, EventArgs e)
        {
            EnergyPictureBox.Visible = true;
            caSpace.EnergyColorsMap.Clear();

            int energyOnGrainsBorders = 0;
            Int32.TryParse(EnergyInsideTextBox.Text, out int energyInsideTheGrains);
            if (HeterogenousDistributionButton.Checked)
            {
                Int32.TryParse(EnergyOnBordersTextBox.Text, out energyOnGrainsBorders);
            }
            else
            {
                energyOnGrainsBorders = energyInsideTheGrains;
            }
            caSpace.SetTheBorders();
            caSpace.DistributeEnergy(energyInsideTheGrains, energyOnGrainsBorders);

            caSpace.EnergyColorsMap.Add(0, new SolidBrush(Color.Blue));
            caSpace.EnergyColorsMap.Add(energyInsideTheGrains, new SolidBrush(Color.Yellow));
            if (energyOnGrainsBorders != energyInsideTheGrains)
                caSpace.EnergyColorsMap.Add(energyOnGrainsBorders, new SolidBrush(Color.Red));

            DrawTheEnergy();
        }

        private void ConstantNucleationButton_CheckedChanged(object sender, EventArgs e)
        {
            InstantNucleationTextBox.Visible = false;
            ConstantNucleationTextBox.Visible = true;
            IncreasingNucleationStartTextBox.Visible = false;
            IncreasingNucleationIncreaseTextBox.Visible = false;
        }

        private void IncreasingNucleationButton_CheckedChanged(object sender, EventArgs e)
        {
            InstantNucleationTextBox.Visible = false;
            ConstantNucleationTextBox.Visible = false;
            IncreasingNucleationStartTextBox.Visible = true;
            IncreasingNucleationIncreaseTextBox.Visible = true;
        }

        private void InstantNucleationButton_CheckedChanged(object sender, EventArgs e)
        {
            InstantNucleationTextBox.Visible = true;
            ConstantNucleationTextBox.Visible = false;
            IncreasingNucleationStartTextBox.Visible = false;
            IncreasingNucleationIncreaseTextBox.Visible = false;
        }

        private void StartRecrystallizationButton_Click(object sender, EventArgs e)
        {
            IncreaseOfRecrystallizedNucleons = 0;
            if (InstantNucleationButton.Checked)
                Int32.TryParse(InstantNucleationTextBox.Text, out NumberOfRecrystallizedNucleons);
            else if (ConstantNucleationButton.Checked)
            {
                Int32.TryParse(ConstantNucleationTextBox.Text, out NumberOfRecrystallizedNucleons);
            }
            else
            {
                Int32.TryParse(IncreasingNucleationStartTextBox.Text, out NumberOfRecrystallizedNucleons);
                Int32.TryParse(IncreasingNucleationIncreaseTextBox.Text, out IncreaseOfRecrystallizedNucleons);
            }

            if (NucleonsAnywhereButton.Checked)
                NucleonsOnBorders = false;
            else
                NucleonsOnBorders = true;

            DrawTheSpace();
            DrawTheEnergy();
            DisableButtons();
            RecrystallizationTimer.Enabled = true;
        }

        private void RecrystallizationTimer_Tick(object sender, EventArgs e)
        {
            if (!caSpace.RecrystallizationIteration(NumberOfRecrystallizedNucleons, NucleonsOnBorders) /*|| !Recrystallization*/)
            {
                EnableButtons();
                RecrystallizationTimer.Enabled = false;
            }

            if (InstantNucleationButton.Checked)
                NumberOfRecrystallizedNucleons = 0;
            else if (IncreasingNucleationButton.Checked)
            {
                NumberOfRecrystallizedNucleons += IncreaseOfRecrystallizedNucleons;
            }

            DrawTheEnergy();
            DrawTheSpace();
        }
    }
}
