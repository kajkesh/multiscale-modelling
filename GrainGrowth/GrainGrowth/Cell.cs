﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrainGrowth
{
    class Cell
    {
        private int GrainNumber;
        private int Row;
        private int Column;
        private bool IsOnBorder;
        private bool IsRecrystallized;
        private int Energy;
        private Neighborhood Neighbors;
        private static readonly Random GetRandom = new Random();

        public Cell(int grainNumber)
        {
            GrainNumber = grainNumber;
        }

        public Cell(int row, int column, int grainNumber)
        {
            Row = row;
            Column = column;
            GrainNumber = grainNumber;
            IsOnBorder = false;
            Energy = 0;
        }

        public int GetGrainNumber()
        {
            return GrainNumber;
        }

        public void SetGrainNumber(int grainNumber)
        {
            GrainNumber = grainNumber;
        }

        public int GetRow()
        {
            return Row;
        }

        public int GetColumn()
        {
            return Column;
        }

        public int GetEnergy()
        {
            return Energy;
        }

        public void SetEnergy(int energy)
        {
            Energy = energy;
        }

        public bool IsCellOnBorder()
        {
            return IsOnBorder;
        }

        public bool IsCellRecrystallized()
        {
            return IsRecrystallized;
        }

        public void SetRecrystallized()
        {
            IsRecrystallized = true;
        }

        public void ZeroTheCell()
        {
            SetGrainNumber(-1);
            IsOnBorder = false;
            IsRecrystallized = false;
        }

        public void SetNeighbors(int maxWidth, int maxHeight)
        {
            int upper = Row - 1 < 0 ? maxHeight - 1 : Row - 1;
            int lower = Row + 1 >= maxHeight ? 0 : Row + 1;
            int right = Column + 1 >= maxWidth ? 0 : Column + 1;
            int left = Column - 1 < 0 ? maxWidth - 1 : Column - 1;
            Neighbors = new Neighborhood(upper, right, lower, left);
        }

        public void RecrystallizedNewGrainNumber(Cell[,] cells)
        {
            Dictionary<int, int> recrystallizedNeighbors = CalculateRecrystallizedNeighbors(cells);
                        
            if (recrystallizedNeighbors.Count > 0)
            {
                //Console.WriteLine("Recrystallized neighbors: " + recrystallizedNeighbors.Count);
                Dictionary<int, int> neighbors = CalculateNumberOfNeighbors(cells, NeighborhoodType.Moore);

                int energy = 0;
                foreach (KeyValuePair<int, int> entry in neighbors)
                {
                    if (entry.Key != GetGrainNumber())
                        energy += entry.Value;
                }
                energy += Energy;

                if (energy > 0)
                {
                    int previousGrainNumber = GetGrainNumber();

                    SetGrainNumber(recrystallizedNeighbors.ElementAt(GetRandom.Next(0, recrystallizedNeighbors.Count)).Key);
                    //Console.WriteLine("Grain number changed to: " + GetGrainNumber());

                    Dictionary<int, int> newNeighbors = CalculateNumberOfNeighbors(cells, NeighborhoodType.Moore);

                    int newEnergy = 0;
                    foreach (KeyValuePair<int, int> entry in newNeighbors)
                    {
                        if (entry.Key != GetGrainNumber())
                            newEnergy += entry.Value;
                    }

                    if (newEnergy > energy)
                    {
                        SetGrainNumber(previousGrainNumber);
                    }
                }
            }
        }

        public void RandomNewGrainNumber(Cell[,] cells)
        {
            Dictionary<int, int> neighbors = CalculateNumberOfNeighbors(cells, NeighborhoodType.Moore);
            int energy = 0;
            foreach (KeyValuePair<int, int> entry in neighbors)
            {
                if (entry.Key != GetGrainNumber())
                    energy += entry.Value;
            }
            if (energy > 0)
            {
                int previousGrainNumber = GetGrainNumber();
                do
                {
                    SetGrainNumber(neighbors.ElementAt(GetRandom.Next(0, neighbors.Count)).Key);
                } while (GetGrainNumber() < -1);
                Dictionary<int, int> newNeighbors = CalculateNumberOfNeighbors(cells, NeighborhoodType.Moore);
                int newEnergy = 0;
                foreach (KeyValuePair<int, int> entry in newNeighbors)
                {
                    if (entry.Key != GetGrainNumber())
                        newEnergy += entry.Value;
                }
                if (newEnergy > energy)
                {
                    SetGrainNumber(previousGrainNumber);
                }
            }
        }

        public Dictionary<int, int> CalculateRecrystallizedNeighbors(Cell[,] cells)
        {
            Dictionary<int, int> numberOfNeighbors = new Dictionary<int, int>();

            if (cells[Neighbors.Upper, Column].IsCellRecrystallized())
            {
                if (numberOfNeighbors.TryGetValue(cells[Neighbors.Upper, Column].GetGrainNumber(), out int number))
                    numberOfNeighbors[cells[Neighbors.Upper, Column].GetGrainNumber()] += 1;
                else
                    numberOfNeighbors.Add(cells[Neighbors.Upper, Column].GetGrainNumber(), 1);
                //Console.WriteLine("Upper neighbor: " + cells[Neighbors.Upper, Column].GetGrainNumber());
            }
            if (cells[Neighbors.Lower, Column].IsCellRecrystallized())
            {
                if (numberOfNeighbors.TryGetValue(cells[Neighbors.Lower, Column].GetGrainNumber(), out int number))
                    numberOfNeighbors[cells[Neighbors.Lower, Column].GetGrainNumber()] += 1;
                else
                    numberOfNeighbors.Add(cells[Neighbors.Lower, Column].GetGrainNumber(), 1);
                //Console.WriteLine("Lower neighbor: " + cells[Neighbors.Lower, Column].GetGrainNumber());
            }
            if (cells[Row, Neighbors.Right].IsCellRecrystallized())
            {
                if (numberOfNeighbors.TryGetValue(cells[Row, Neighbors.Right].GetGrainNumber(), out int number))
                    numberOfNeighbors[cells[Row, Neighbors.Right].GetGrainNumber()] += 1;
                else
                    numberOfNeighbors.Add(cells[Row, Neighbors.Right].GetGrainNumber(), 1);
                //Console.WriteLine("Right neighbor: " + cells[Row, Neighbors.Right].GetGrainNumber());
            }
            if (cells[Row, Neighbors.Left].IsCellRecrystallized())
            {
                if (numberOfNeighbors.TryGetValue(cells[Row, Neighbors.Left].GetGrainNumber(), out int number))
                    numberOfNeighbors[cells[Row, Neighbors.Left].GetGrainNumber()] += 1;
                else
                    numberOfNeighbors.Add(cells[Row, Neighbors.Left].GetGrainNumber(), 1);
                //Console.WriteLine("Left neighbor: " + cells[Row, Neighbors.Left].GetGrainNumber());
            }
            if (cells[Neighbors.Upper, Neighbors.Right].IsCellRecrystallized())
            {
                if (numberOfNeighbors.TryGetValue(cells[Neighbors.Upper, Neighbors.Right].GetGrainNumber(), out int number))
                    numberOfNeighbors[cells[Neighbors.Upper, Neighbors.Right].GetGrainNumber()] += 1;
                else
                    numberOfNeighbors.Add(cells[Neighbors.Upper, Neighbors.Right].GetGrainNumber(), 1);
            }
            if (cells[Neighbors.Lower, Neighbors.Right].IsCellRecrystallized())
            {
                if (numberOfNeighbors.TryGetValue(cells[Neighbors.Lower, Neighbors.Right].GetGrainNumber(), out int number))
                    numberOfNeighbors[cells[Neighbors.Lower, Neighbors.Right].GetGrainNumber()] += 1;
                else
                    numberOfNeighbors.Add(cells[Neighbors.Lower, Neighbors.Right].GetGrainNumber(), 1);
            }
            if (cells[Neighbors.Lower, Neighbors.Left].IsCellRecrystallized())
            {
                if (numberOfNeighbors.TryGetValue(cells[Neighbors.Lower, Neighbors.Left].GetGrainNumber(), out int number))
                    numberOfNeighbors[cells[Neighbors.Lower, Neighbors.Left].GetGrainNumber()] += 1;
                else
                    numberOfNeighbors.Add(cells[Neighbors.Lower, Neighbors.Left].GetGrainNumber(), 1);
            }
            if (cells[Neighbors.Upper, Neighbors.Left].IsCellRecrystallized())
            {
                if (numberOfNeighbors.TryGetValue(cells[Neighbors.Upper, Neighbors.Left].GetGrainNumber(), out int number))
                    numberOfNeighbors[cells[Neighbors.Upper, Neighbors.Left].GetGrainNumber()] += 1;
                else
                    numberOfNeighbors.Add(cells[Neighbors.Upper, Neighbors.Left].GetGrainNumber(), 1);
            }
            return numberOfNeighbors;
        }

        public Dictionary<int, int> CalculateNumberOfNeighbors(Cell[,] cells, NeighborhoodType type)
        {
            Dictionary<int, int> numberOfNeighbors = new Dictionary<int, int>();
            int upperGrainNumber = cells[Neighbors.Upper, Column].GetGrainNumber();
            int lowerGrainNumber = cells[Neighbors.Lower, Column].GetGrainNumber();
            int rightGrainNumber = cells[Row, Neighbors.Right].GetGrainNumber();
            int leftGrainNumber = cells[Row, Neighbors.Left].GetGrainNumber();
            int upperRightGrainNumber = cells[Neighbors.Upper, Neighbors.Right].GetGrainNumber();
            int lowerRightGrainNumber = cells[Neighbors.Lower, Neighbors.Right].GetGrainNumber();
            int lowerLeftGrainNumber = cells[Neighbors.Lower, Neighbors.Left].GetGrainNumber();
            int upperLeftGrainNumber = cells[Neighbors.Upper, Neighbors.Left].GetGrainNumber();

            switch (type)
            {
                case NeighborhoodType.VonNeumann:
                    if (upperGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(upperGrainNumber, out int number))
                            numberOfNeighbors[upperGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(upperGrainNumber, 1);
                    }
                    if (lowerGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(lowerGrainNumber, out int number))
                            numberOfNeighbors[lowerGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(lowerGrainNumber, 1);
                    }
                    if (rightGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(rightGrainNumber, out int number))
                            numberOfNeighbors[rightGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(rightGrainNumber, 1);
                    }
                    if (leftGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(leftGrainNumber, out int number))
                            numberOfNeighbors[leftGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(leftGrainNumber, 1);
                    }
                    break;
                case NeighborhoodType.Moore:
                    if (upperGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(upperGrainNumber, out int number))
                            numberOfNeighbors[upperGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(upperGrainNumber, 1);
                    }
                    if (lowerGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(lowerGrainNumber, out int number))
                            numberOfNeighbors[lowerGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(lowerGrainNumber, 1);
                    }
                    if (rightGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(rightGrainNumber, out int number))
                            numberOfNeighbors[rightGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(rightGrainNumber, 1);
                    }
                    if (leftGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(leftGrainNumber, out int number))
                            numberOfNeighbors[leftGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(leftGrainNumber, 1);
                    }
                    if (upperRightGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(upperRightGrainNumber, out int number))
                            numberOfNeighbors[upperRightGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(upperRightGrainNumber, 1);
                    }
                    if (upperLeftGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(upperLeftGrainNumber, out int number))
                            numberOfNeighbors[upperLeftGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(upperLeftGrainNumber, 1);
                    }
                    if (lowerRightGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(lowerRightGrainNumber, out int number))
                            numberOfNeighbors[lowerRightGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(lowerRightGrainNumber, 1);
                    }
                    if (lowerLeftGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(lowerLeftGrainNumber, out int number))
                            numberOfNeighbors[lowerLeftGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(lowerLeftGrainNumber, 1);
                    }
                    break;
                case NeighborhoodType.FurtherMoore:
                    if (upperRightGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(upperRightGrainNumber, out int number))
                            numberOfNeighbors[upperRightGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(upperRightGrainNumber, 1);
                    }
                    if (upperLeftGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(upperLeftGrainNumber, out int number))
                            numberOfNeighbors[upperLeftGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(upperLeftGrainNumber, 1);
                    }
                    if (lowerRightGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(lowerRightGrainNumber, out int number))
                            numberOfNeighbors[lowerRightGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(lowerRightGrainNumber, 1);
                    }
                    if (lowerLeftGrainNumber > -1)
                    {
                        if (numberOfNeighbors.TryGetValue(lowerLeftGrainNumber, out int number))
                            numberOfNeighbors[lowerLeftGrainNumber] += 1;
                        else
                            numberOfNeighbors.Add(lowerLeftGrainNumber, 1);
                    }
                    break;
                default:
                    break;
            }
            return numberOfNeighbors;
        }

        public int CalculateNewGrainNumber(Cell[,] cells, NeighborhoodType type, int minimumNumberOfNeighbors)
        {
            Dictionary<int, int> numberOfNeighbors = CalculateNumberOfNeighbors(cells, type);
            if (numberOfNeighbors.Count == 0)
            {
                return -1;
            }
            else
            {
                int numberOfMaxNeighbors = numberOfNeighbors.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
                return numberOfNeighbors[numberOfMaxNeighbors] >= minimumNumberOfNeighbors ? numberOfMaxNeighbors : -1;
            }
        }

        public void CheckBorders(Cell[,] cells)
        {
            if (cells[Neighbors.Upper, Column].GetGrainNumber() != GrainNumber
                || cells[Neighbors.Lower, Column].GetGrainNumber() != GrainNumber
                || cells[Row, Neighbors.Right].GetGrainNumber() != GrainNumber
                || cells[Row, Neighbors.Left].GetGrainNumber() != GrainNumber
                || cells[Neighbors.Upper, Neighbors.Right].GetGrainNumber() != GrainNumber
                || cells[Neighbors.Lower, Neighbors.Right].GetGrainNumber() != GrainNumber
                || cells[Neighbors.Lower, Neighbors.Left].GetGrainNumber() != GrainNumber
                || cells[Neighbors.Upper, Neighbors.Left].GetGrainNumber() != GrainNumber)
                IsOnBorder = true;
        }

        public bool CheckIfIsBorder(Cell[,] cells, bool isSelected = false)
        {
            if (GrainNumber > -1)
            {
                int upperRightGrainNumber = cells[Neighbors.Upper, Neighbors.Right].GetGrainNumber();
                int lowerRightGrainNumber = cells[Neighbors.Lower, Neighbors.Right].GetGrainNumber();
                int lowerGrainNumber = cells[Neighbors.Lower, Column].GetGrainNumber();
                int rightGrainNumber = cells[Row, Neighbors.Right].GetGrainNumber();
                if ((rightGrainNumber > -1 && rightGrainNumber != GrainNumber)
                    || (lowerGrainNumber > -1 && lowerGrainNumber != GrainNumber)
                    || (lowerRightGrainNumber > -1 && lowerRightGrainNumber != GrainNumber)
                    || (upperRightGrainNumber > -1 && upperRightGrainNumber != GrainNumber))
                    return true;

                if (isSelected)
                {
                    int upperGrainNumber = cells[Neighbors.Upper, Column].GetGrainNumber();
                    int lowerLeftGrainNumber = cells[Neighbors.Lower, Neighbors.Left].GetGrainNumber();
                    int upperLeftGrainNumber = cells[Neighbors.Upper, Neighbors.Left].GetGrainNumber();
                    int leftGrainNumber = cells[Row, Neighbors.Left].GetGrainNumber();
                    if ((leftGrainNumber > -1 && leftGrainNumber != GrainNumber)
                    || (upperLeftGrainNumber > -1 && upperLeftGrainNumber != GrainNumber)
                    || (lowerLeftGrainNumber > -1 && lowerLeftGrainNumber != GrainNumber)
                    || (upperRightGrainNumber > -1 && upperRightGrainNumber != GrainNumber))
                        return true;
                }
            }
            return false;
        }
    }
}
