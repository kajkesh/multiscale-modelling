﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrainGrowth
{
    enum NeighborhoodType
    {
        VonNeumann,
        Moore,
        FurtherMoore
    };

    class Neighborhood
    {
        public int Upper;
        public int Lower;
        public int Left;
        public int Right;

        public Neighborhood(int upper, int right, int lower, int left)
        {
            Upper = upper;
            Right = right;
            Lower = lower;
            Left = left;
        }
    }
}
