# MultiscaleModelling

---

##Overview

The purpose of this project was to create a program to generate different microstructures
based on given parameters with a use of cellular automata. The mentioned parameters are
for example number of initial nucleons, number, shape and size of added inclusions or
different transition rules.

---

##Features

1. Simple grain growth with von Neumann transition rule
2. Import and export of the generated structure to txt/bmp files
3. Adding inclusions at the beginning and at the end of the simulation. The inclusions are not affected by the grain growth
4. Grain growth with extended Moore transition rule acting on given probability
5. Dual-phase and substructure generation with grains regrowth
6. Calculation of grains' boundaries percentage share in the microstructure, regrowth of the grains within the drawn borders
7. Monte Carlo algorithm based grain growth